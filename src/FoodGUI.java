import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    private JPanel root;
    private JButton tempuraButton;
    private JButton sahimiButton;
    private JButton tamagoyakiButton;
    private JButton karageButton;
    private JButton yakitoriButton;
    private JButton beerButton;
    private JTextPane orderList;
    private JButton CheckoutButton;
    private JLabel payment;
    private JLabel tempura_field;
    private JLabel sashimi_field;
    private JLabel tamagoyaki_field;
    private JLabel karage_field;
    private JLabel yakitori_field;
    private JLabel beer_field;
    private JButton CancelBtton;
    private int tempura=500;
    private int sashimi=1000;
    private int tamagoyaki=350;
    private int karage=450;
    private int yakitori=600;
    private int beer=400;
    private int allprice=0;
    int select=0;
    private String currentText;
    private String [] selectFig={"1","2","3","4","cancel"};


    void add_orderList(String choice,int price){
        if(select==5) {
        }else {
            price*=select;
            currentText = orderList.getText();
            orderList.setText(currentText + choice + " x"+select+"   " + price + "yen\n");
        }
    }

    void payment(int price){
        if(select==5) {
        }else {
            price*=select;
            allprice += price;
            payment.setText("Total: " + allprice + " Yen");
        }
    }

    void order(String food) {
        select=JOptionPane.showOptionDialog(null,
                "would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                selectFig,
                selectFig[0]
                );
        select++;
        if(select==5) {
        }else {
            JOptionPane.showMessageDialog(null,
                    "Ordered of "+food+" for "+select);
        }
    }
    public FoodGUI() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("tempura");
                /*if(tempura_fig>=4){
                    all_tempura=(tempura*tempura_fig)*0.2;
                    allprice-=all_tempura;
                };*/
                add_orderList("tempura",tempura);
                payment(tempura);

            }
        });
        sahimiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sashimi");
                add_orderList("Sashimi",sashimi);
                payment(sashimi);
            }
        });
        tamagoyakiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tamagoyaki");
                add_orderList("Tamagoyaki",tamagoyaki);
                payment(tamagoyaki);
            }
        });
        karageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("karage");
                add_orderList("karage",karage);
                payment(karage);
            }
        });
        yakitoriButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakitori");
                add_orderList("Yakitori",yakitori);
                payment(yakitori);
            }
        });
        beerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Beer");
                add_orderList("Beer",beer);
                payment(beer);
            }
        });

        CancelBtton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(currentText==null){
                    JOptionPane.showMessageDialog(null,
                            "Not Ordered yet");
                }else{
                    int cancel=JOptionPane.showConfirmDialog(null,
                            "Delete the all ordered items?",
                            "Cancel Message",
                            JOptionPane.YES_NO_OPTION);
                    if(cancel==0){
                        JOptionPane.showMessageDialog(null, "Deleted the all ordered items");
                        orderList.setText("");
                        allprice=0;
                        payment(allprice);
                        currentText=null;

                    }
                }

            }
        });

        CheckoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you!The total price is "+allprice+" yen.");
                    orderList.setText("");
                    allprice=0;
                    payment(allprice);
                    currentText=null;

                }

            }
        });
        tempuraButton.setIcon(new ImageIcon(
                this.getClass().getResource("tempura.jpg")
        ));
        sahimiButton.setIcon(new ImageIcon(
                this.getClass().getResource("sashimi.jpg")
        ));
        tamagoyakiButton.setIcon(new ImageIcon(
                this.getClass().getResource("tamagoyaki.jpg")
        ));
        karageButton.setIcon(new ImageIcon(
                this.getClass().getResource("karaage.jpg")
        ));
        yakitoriButton.setIcon(new ImageIcon(
                this.getClass().getResource("yakitori.jpg")
        ));
        beerButton.setIcon(new ImageIcon(
                this.getClass().getResource("beer.jpg")
        ));

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
